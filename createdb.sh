#!/bin/bash
# dirty script to generate over mariadb/mysql a database, username and password with privileges over the database
dbpass=$(date +%s | sha256sum | base64 | head -c 15)
echo -n "Enter mysql admin user password: "; stty -echo; read mysqlpass; stty echo; echo
echo "introduce nombre para la nueva base de datos: "
read nombre
mysql -u root  -p$mysqlpass  <<EOF
SHOW DATABASES;
CREATE USER $nombre@'localhost' IDENTIFIED BY '$dbpass';
CREATE DATABASE $nombre;
GRANT ALL PRIVILEGES ON $nombre.* TO '$nombre'@'localhost';
FLUSH PRIVILEGES;
EOF

echo -e "\n\n\n\n"
echo -e "#############################"
echo database name:   $nombre
echo user name:    $nombre
echo database password: $dbpass
echo -e "#############################"
echo -e "\n\n\n\n"